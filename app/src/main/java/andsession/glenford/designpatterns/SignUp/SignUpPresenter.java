package andsession.glenford.designpatterns.SignUp;

/**
 * Created by glenford on 21/7/16.
 */
public interface SignUpPresenter {
    void signUp(String username, String password);
}
