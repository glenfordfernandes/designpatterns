package andsession.glenford.designpatterns.SignUp;

/**
 * Created by glenford on 21/7/16.
 */
public interface SignUpView {
    void displayStatus(String status);
    void showProgress(boolean show);
}
