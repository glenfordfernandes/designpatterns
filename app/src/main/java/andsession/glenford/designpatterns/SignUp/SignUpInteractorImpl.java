package andsession.glenford.designpatterns.SignUp;

import android.os.Handler;

import java.util.Random;

/**
 * Created by glenford on 21/7/16.
 */
public class SignUpInteractorImpl implements SignUpInteractor {

    @Override
    public void performRemoteSignUp(String username, String password,
                                    final OnSignUpFinishedListener onSignUpFinishedListener) {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                Random rand = new Random();
                int oneOrZero = rand.nextInt(1);
                if (oneOrZero < 1)
                    onSignUpFinishedListener.onError();
                else
                    onSignUpFinishedListener.onSuccess();
            }
        }, 2000);
    }
}
