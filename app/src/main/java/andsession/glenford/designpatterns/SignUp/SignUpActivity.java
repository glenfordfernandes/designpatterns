package andsession.glenford.designpatterns.SignUp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import andsession.glenford.designpatterns.SignUp.SignUpPresenterImpl;
import andsession.glenford.designpatterns.SignUp.SignUpView;
import andsession.glenford.designpatterns.R;


public class SignUpActivity extends Activity implements SignUpView, View.OnClickListener {

    private SignUpPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mPresenter = new SignUpPresenterImpl(this);
        findViewById(R.id.btn_sign_up).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up:
                String username = ((EditText) findViewById(R.id.edt_username)).getText().toString();
                String password = ((EditText) findViewById(R.id.edt_password)).getText().toString();
                mPresenter.signUp(username, password);
                break;
        }
    }

    @Override
    public void displayStatus(String status) {
        Toast.makeText(this, status, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress(boolean show) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }
}
