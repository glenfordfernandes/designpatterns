package andsession.glenford.designpatterns.SignUp;

/**
 * Created by glenford on 21/7/16.
 */
public class SignUpPresenterImpl implements SignUpPresenter, SignUpInteractor.OnSignUpFinishedListener {

    private final SignUpView mSignUpView;
    private final SignUpInteractorImpl mSignUpInteractor;

    public SignUpPresenterImpl(SignUpView signUpView) {
        mSignUpView = signUpView;
        mSignUpInteractor = new SignUpInteractorImpl();
    }

    @Override
    public void signUp(String username, String password) {
        if (mSignUpView != null)
            mSignUpView.showProgress(true);
        mSignUpInteractor.performRemoteSignUp(username, password, this);
    }

    @Override
    public void onError() {
        if (mSignUpView != null) {
            mSignUpView.showProgress(false);
            mSignUpView.displayStatus("Sign Up failed.");
        }
    }

    @Override
    public void onSuccess() {
        if (mSignUpView != null) {
            mSignUpView.showProgress(false);
            mSignUpView.displayStatus("Sign Up Successful.");
        }
    }
}
