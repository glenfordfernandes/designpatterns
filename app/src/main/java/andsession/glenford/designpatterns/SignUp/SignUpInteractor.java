package andsession.glenford.designpatterns.SignUp;

/**
 * Created by glenford on 21/7/16.
 */
public interface SignUpInteractor {

    interface OnSignUpFinishedListener {
        void onError();
        void onSuccess();
    }

    void performRemoteSignUp(String username, String password,
                             OnSignUpFinishedListener onSignUpFinishedListener);
}
