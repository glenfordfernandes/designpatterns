package andsession.glenford.designpatterns.abstractFactory;

import android.content.Context;
import android.graphics.drawable.Drawable;

import andsession.glenford.designpatterns.factoryMethod.BasicTextView;

/**
 * Created by glenford on 1/6/16.
 */
abstract public class AbstractFactory {
    public enum Shape {OVAL, RECTANGLE}
    public enum FactoryType {TEXTVIEW, DRAWABLE}
    public abstract BasicTextView getStyledTextView(Context context, int style);
    public abstract Drawable getDrawable(Context context, Shape shape);
}
