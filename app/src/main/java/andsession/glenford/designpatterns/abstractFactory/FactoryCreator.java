package andsession.glenford.designpatterns.abstractFactory;

import andsession.glenford.designpatterns.factoryMethod.DrawableFactory;
import andsession.glenford.designpatterns.factoryMethod.TextViewFactory;

/**
 * Created by glenford on 1/6/16.
 */
public class FactoryCreator {
    public static AbstractFactory createFactory(AbstractFactory.FactoryType factoryType) {
        switch (factoryType) {
            case TEXTVIEW:
                return new DrawableFactory();
            case DRAWABLE:
                return new TextViewFactory();
        }
        return null;
    }
}
