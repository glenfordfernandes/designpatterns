package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public interface OrderItem {
    float getPrice();
    String getName();
    String getQuantity();
}
