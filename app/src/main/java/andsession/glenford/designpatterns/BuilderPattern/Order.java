package andsession.glenford.designpatterns.BuilderPattern;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by glenford on 1/6/16.
 */
public class Order {

    private static final String TAG = Order.class.getSimpleName();
    private List<OrderItem> mOrderItemList;

    private Order(OrderBuilder orderBuilder) {
        this.mOrderItemList = orderBuilder.orderItemList;
    }

    public void showOrder() {
        double grossTotal = 0;
        Log.d(TAG, "================================");
        for (OrderItem orderItem: mOrderItemList) {
            Log.d(TAG, "Item: "+ orderItem.getName());
            Log.d(TAG, "Price: "+ orderItem.getPrice());
            Log.d(TAG, "Quantity: "+ orderItem.getQuantity());
            grossTotal += orderItem.getPrice();
        }
        Log.d(TAG, "================================");
        Log.d(TAG, "Total: "+ grossTotal);
    }

    public static class OrderBuilder {
        private List<OrderItem> orderItemList;

        public OrderBuilder() {
            this.orderItemList = new ArrayList<>();
        }

        public OrderBuilder addToOrder(OrderItem item) {
            this.orderItemList.add(item);
            return this;
        }

        public Order buildOrder() {
            return new Order(this);
        }
    }
}
