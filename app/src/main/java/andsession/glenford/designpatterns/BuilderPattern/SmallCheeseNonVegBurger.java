package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public class SmallCheeseNonVegBurger extends NonVegBurger {

    @Override
    public String getName() {
        return mBurgerNameStandard.concat("Cheese Burger");
    }

    @Override
    public float getPrice() {
        return mBurgerStandardPrice + 80;
    }

    @Override
    public String getQuantity() {
        return "Small";
    }
}
