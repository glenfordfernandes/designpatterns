package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public abstract class Drink implements OrderItem {
    public float mDrinkStandardPrice = 20;
    public abstract float getPrice();
}
