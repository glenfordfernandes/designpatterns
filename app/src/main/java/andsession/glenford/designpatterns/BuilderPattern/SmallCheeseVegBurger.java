package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public class SmallCheeseVegBurger extends VegBurger {

    @Override
    public String getName() {
        return mBurgerNameStandard.concat("Cheese Burger");
    }

    @Override
    public float getPrice() {
        return mBurgerStandardPrice + 50;
    }

    @Override
    public String getQuantity() {
        return "Small";
    }
}
