package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public class KingCheeseVegBurger extends VegBurger {

    @Override
    public String getName() {
        return mBurgerNameStandard.concat("King Cheese Burger");
    }

    @Override
    public float getPrice() {
        return mBurgerStandardPrice + 100;
    }

    @Override
    public String getQuantity() {
        return "Large";
    }
}
