package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public abstract class Coke extends Drink {

    public String mNameStandard = "Coke";

    @Override
    public abstract String getName();

    @Override
    public abstract float getPrice();

    @Override
    public abstract String getQuantity();
}
