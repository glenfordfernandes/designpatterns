package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public class SmallCoke extends Coke {

    @Override
    public String getName() {
        return mNameStandard.concat("mini");
    }

    @Override
    public float getPrice() {
        return mDrinkStandardPrice;
    }

    @Override
    public String getQuantity() {
        return "Small";
    }
}
