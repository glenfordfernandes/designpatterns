package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public class MediumCheeseNonVegBurger extends NonVegBurger {

    @Override
    public String getName() {
        return super.getName().concat(" Cheese Burger");
    }

    @Override
    public float getPrice() {
        return mBurgerStandardPrice + 100;
    }

    @Override
    public String getQuantity() {
        return "Medium";
    }
}
