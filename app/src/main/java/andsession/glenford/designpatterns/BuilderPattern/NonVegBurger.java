package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public abstract class NonVegBurger extends Burger {

    @Override
    public String getName() {
        return mBurgerNameStandard.concat(" Non-Veg");
    }

    @Override
    public abstract float getPrice();

    @Override
    public abstract String getQuantity();
}
