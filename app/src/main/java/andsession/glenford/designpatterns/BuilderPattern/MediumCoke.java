package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public class MediumCoke extends Coke {

    @Override
    public String getName() {
        return mNameStandard;
    }

    @Override
    public float getPrice() {
        return mDrinkStandardPrice + 50;
    }

    @Override
    public String getQuantity() {
        return "Medium";
    }
}
