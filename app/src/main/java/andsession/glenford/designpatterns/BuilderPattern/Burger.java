package andsession.glenford.designpatterns.BuilderPattern;

/**
 * Created by glenford on 1/6/16.
 */
public abstract class Burger implements OrderItem {
    public float mBurgerStandardPrice = 200;
    public String mBurgerNameStandard = "Burger";
    public abstract float getPrice();
}
