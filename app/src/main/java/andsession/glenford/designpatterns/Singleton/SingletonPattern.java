package andsession.glenford.designpatterns.Singleton;

/**
 * Created by glenford on 20/6/16.
 */
public class SingletonPattern {

    private static SingletonPattern ourInstance;

    public static SingletonPattern getInstance() {
        if (ourInstance == null) {
            ourInstance = new SingletonPattern();
        }
        return ourInstance;
    }

    private SingletonPattern() {
    }
}
