package andsession.glenford.designpatterns.BehaviouralPatterns.ObserverPattern;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import andsession.glenford.designpatterns.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragThree extends Fragment implements NotificationManager.Observer {

    private static final String TAG = FragThree.class.getSimpleName();
    private TextView mTv;

    public FragThree() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NotificationManager.getInstance().addObserver(Notification.NOTIFY_UPDATE, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_one, container, false);
        mTv = (TextView) view.findViewById(R.id.tv);
        mTv.setText("Fragment Three");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle data = new Bundle();
                data.putString("data", TAG);
                NotificationManager.getInstance().notifyObservers(Notification.NOTIFY_UPDATE, data);
            }
        });

        return view;
    }

    @Override
    public void update(Notification notificationName, Bundle data) {
        if (notificationName == Notification.NOTIFY_UPDATE) {
            String dataString = data.getString("data");
            mTv.setText(dataString);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NotificationManager.getInstance().removeObserver(Notification.NOTIFY_UPDATE, this);
    }
}
