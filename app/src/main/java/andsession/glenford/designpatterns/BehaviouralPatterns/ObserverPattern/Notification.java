package andsession.glenford.designpatterns.BehaviouralPatterns.ObserverPattern;

/**
 * Created by glenford on 20/7/16.
 */
public enum Notification {
    NOTIFY_UPDATE
}
