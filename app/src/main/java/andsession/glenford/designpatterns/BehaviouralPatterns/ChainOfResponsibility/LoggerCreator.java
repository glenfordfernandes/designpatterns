package andsession.glenford.designpatterns.BehaviouralPatterns.ChainOfResponsibility;

/**
 * Created by glenford on 20/7/16.
 */
public class LoggerCreator {

    public static AbstractLogger CreateLoggerChain() {
        AbstractLogger debugLogger = new DebugLogger(AbstractLogger.DEBUG);
        AbstractLogger infoLogger = new InfoLogger(AbstractLogger.INFO);
        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);

        debugLogger.setNextLogger(infoLogger);
        infoLogger.setNextLogger(errorLogger);
        return debugLogger;
    }
}
