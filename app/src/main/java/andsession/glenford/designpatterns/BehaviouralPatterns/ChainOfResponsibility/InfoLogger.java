package andsession.glenford.designpatterns.BehaviouralPatterns.ChainOfResponsibility;

import android.util.Log;

/**
 * Created by glenford on 20/7/16.
 */
public class InfoLogger extends AbstractLogger {

    private static final String TAG = InfoLogger.class.getSimpleName();

    public InfoLogger(int logType) {
        this.level = logType;
    }

    @Override
    protected void write(String message) {
        Log.i(TAG, message);
    }
}
