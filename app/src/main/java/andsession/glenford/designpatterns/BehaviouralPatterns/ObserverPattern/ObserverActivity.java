package andsession.glenford.designpatterns.BehaviouralPatterns.ObserverPattern;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import andsession.glenford.designpatterns.R;

public class ObserverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observor_acitivity);

        FragOne fragOne = new FragOne();
        FragTwo fragTwo = new FragTwo();
        FragThree fragThree = new FragThree();
        FragFour fragFour = new FragFour();

        getSupportFragmentManager().beginTransaction().add(R.id.layoutOne, fragOne).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.layoutOne, fragTwo).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.layoutTwo, fragThree).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.layoutTwo, fragFour).commit();
    }
}
