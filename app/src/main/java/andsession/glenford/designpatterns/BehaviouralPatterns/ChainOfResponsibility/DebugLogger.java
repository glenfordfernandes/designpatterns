package andsession.glenford.designpatterns.BehaviouralPatterns.ChainOfResponsibility;

import android.util.Log;

/**
 * Created by glenford on 20/7/16.
 */
public class DebugLogger extends AbstractLogger {

    private static final String TAG = DebugLogger.class.getSimpleName();

    public DebugLogger(int logType) {
        this.level = logType;
    }

    @Override
    protected void write(String message) {
        Log.d(TAG, message);
    }
}
