package andsession.glenford.designpatterns.BehaviouralPatterns.ChainOfResponsibility;

import android.util.Log;

/**
 * Created by glenford on 20/7/16.
 */
public class ErrorLogger extends AbstractLogger {

    private static final String TAG = ErrorLogger.class.getSimpleName();

    public ErrorLogger(int logType) {
        this.level = logType;
    }

    @Override
    protected void write(String message) {
        Log.e(TAG, message);
    }
}
