package andsession.glenford.designpatterns.facade;

import andsession.glenford.designpatterns.BuilderPattern.Coke;
import andsession.glenford.designpatterns.BuilderPattern.KingCheeseVegBurger;
import andsession.glenford.designpatterns.BuilderPattern.MediumCheeseNonVegBurger;
import andsession.glenford.designpatterns.BuilderPattern.MediumCheeseVegBurger;
import andsession.glenford.designpatterns.BuilderPattern.MediumCoke;
import andsession.glenford.designpatterns.BuilderPattern.NonVegBurger;
import andsession.glenford.designpatterns.BuilderPattern.Order;
import andsession.glenford.designpatterns.BuilderPattern.Pepsi;
import andsession.glenford.designpatterns.BuilderPattern.VegBurger;

/**
 * Created by glenford on 23/6/16.
 */
public class OrderFacade {

    public enum OrderType {REGULAR, MEDIUM, LARGE, FUNKY}

    public static void placeOrder(OrderType orderType) {
        switch (orderType) {
            case REGULAR:
                new Order.OrderBuilder()
                        .addToOrder(new NonVegBurger() {
                            @Override
                            public float getPrice() {
                                return mBurgerStandardPrice;
                            }

                            @Override
                            public String getQuantity() {
                                return "Regular";
                            }
                        })
                        .addToOrder(new VegBurger() {
                            @Override
                            public float getPrice() {
                                return mBurgerStandardPrice;
                            }

                            @Override
                            public String getQuantity() {
                                return "Regular";
                            }
                        })
                        .addToOrder(new Coke() {
                            @Override
                            public String getName() {
                                return mNameStandard;
                            }

                            @Override
                            public float getPrice() {
                                return mDrinkStandardPrice;
                            }

                            @Override
                            public String getQuantity() {
                                return "Regular";
                            }
                        })
                        .buildOrder()
                        .showOrder();
                break;
            case MEDIUM:
                new Order.OrderBuilder()
                        .addToOrder(new MediumCheeseNonVegBurger())
                        .addToOrder(new MediumCheeseVegBurger())
                        .addToOrder(new MediumCoke())
                        .buildOrder()
                        .showOrder();
                break;
            case LARGE:
                new Order.OrderBuilder()
                        .addToOrder(new NonVegBurger() {
                            @Override
                            public float getPrice() {
                                return mBurgerStandardPrice + 100;
                            }

                            @Override
                            public String getQuantity() {
                                return "Large";
                            }
                        })
                        .addToOrder(new VegBurger() {
                            @Override
                            public float getPrice() {
                                return mBurgerStandardPrice + 80;
                            }

                            @Override
                            public String getQuantity() {
                                return "Large";
                            }
                        })
                        .addToOrder(new Coke() {
                            @Override
                            public String getName() {
                                return mNameStandard;
                            }

                            @Override
                            public float getPrice() {
                                return mDrinkStandardPrice + 50;
                            }

                            @Override
                            public String getQuantity() {
                                return "Large";
                            }
                        })
                        .buildOrder()
                        .showOrder();
                break;
            case FUNKY:
                new Order.OrderBuilder()
                        .addToOrder(new MediumCheeseNonVegBurger())
                        .addToOrder(new KingCheeseVegBurger())
                        .addToOrder(new MediumCoke())
                        .addToOrder(new Pepsi() {
                            @Override
                            public String getName() {
                                return "Pepsi Funky";
                            }

                            @Override
                            public float getPrice() {
                                return mDrinkStandardPrice;
                            }

                            @Override
                            public String getQuantity() {
                                return "Funky Pep";
                            }
                        })
                        .buildOrder()
                        .showOrder();
                break;
        }
    }
}
