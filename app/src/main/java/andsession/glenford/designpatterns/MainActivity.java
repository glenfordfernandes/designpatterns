package andsession.glenford.designpatterns;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import andsession.glenford.designpatterns.BehaviouralPatterns.ChainOfResponsibility.AbstractLogger;
import andsession.glenford.designpatterns.BehaviouralPatterns.ChainOfResponsibility.LoggerCreator;
import andsession.glenford.designpatterns.BuilderPattern.MediumCheeseNonVegBurger;
import andsession.glenford.designpatterns.BuilderPattern.Order;
import andsession.glenford.designpatterns.BuilderPattern.SmallCheeseVegBurger;
import andsession.glenford.designpatterns.BuilderPattern.SmallCoke;
import andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern.DecoratedFontFaceTextView;
import andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern.SimpleTextView;
import andsession.glenford.designpatterns.StructuralPatterns.adapter.DrawCircleAdapter;
import andsession.glenford.designpatterns.StructuralPatterns.adapter.DrawHelper;
import andsession.glenford.designpatterns.facade.OrderFacade;
import andsession.glenford.designpatterns.factoryMethod.BasicTextView;
import andsession.glenford.designpatterns.factoryMethod.TextViewFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final LinearLayout textContainer = (LinearLayout) findViewById(R.id.text_container);

        /**
         * Factory pattern
         */
        TextViewFactory textViewFactory = new TextViewFactory();

        BasicTextView basicTextView = textViewFactory.getStyledTextView(this, Typeface.BOLD);
        basicTextView.setText("Hello World !!");
        textContainer.addView(basicTextView);

        basicTextView = textViewFactory.getStyledTextView(this, Typeface.BOLD_ITALIC);
        basicTextView.setText("Hello World !!");
        textContainer.addView(basicTextView);

        /**
         * Decorator pattern
         */
        SimpleTextView simpleTextView = new SimpleTextView(this);
        simpleTextView.setText("Simple Text");
        textContainer.addView(simpleTextView);

        simpleTextView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                view = new DecoratedFontFaceTextView(MainActivity.this, (andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern.View) view);
                textContainer.invalidate();
            }
        });

        /**
         * Builder pattern
         */
        new Order.OrderBuilder()
                .addToOrder(new MediumCheeseNonVegBurger())
                .addToOrder(new SmallCheeseVegBurger())
                .addToOrder(new SmallCoke())

                .buildOrder()
                .showOrder();

        /**
         * Facade Pattern
         */
        OrderFacade.placeOrder(OrderFacade.OrderType.FUNKY);
        OrderFacade.placeOrder(OrderFacade.OrderType.LARGE);
        OrderFacade.placeOrder(OrderFacade.OrderType.REGULAR);

        /**
         * Composite Pattern
         */
        LinearLayout compositeLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.composite_layout, null);
        showChildren(compositeLayout);

        /**
         * Chained Patterns
         */
        AbstractLogger initialLogger = LoggerCreator.CreateLoggerChain();
        initialLogger.logMessage(AbstractLogger.DEBUG, "Debug log");
        initialLogger.logMessage(AbstractLogger.ERROR, "Error log");
        initialLogger.logMessage(AbstractLogger.INFO, "Info log");
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        final FrameLayout view = (FrameLayout) findViewById(R.id.emptyView);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                /**
                 * Adapter Pattern
                 */
                DrawHelper circleDrawer = new DrawCircleAdapter(view.getContext());
                circleDrawer.draw(view);
            }
        });
    }

    private void showChildren(ViewGroup compositeLayout) {
        int childCount =  compositeLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View view = compositeLayout.getChildAt(i);
            Log.d(TAG, compositeLayout.toString()+ " contains: "+ view.toString());
            if (view instanceof ViewGroup)
                showChildren((ViewGroup) view);
        }
    }
}
