package andsession.glenford.designpatterns.factoryMethod;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;

import andsession.glenford.designpatterns.abstractFactory.AbstractFactory;

/**
 * Created by glenford on 1/6/16.
 */
public class DrawableFactory extends AbstractFactory {

    @Override
    public BasicTextView getStyledTextView(Context context, int style) {
        return null;
    }

    @Override
    public Drawable getDrawable(Context context, Shape shape) {
        Drawable drawable = null;
        switch (shape) {
            case OVAL:
                drawable = new ShapeDrawable(new OvalShape());
                break;
            case RECTANGLE:
                drawable = new ShapeDrawable(new RectShape());
                break;
        }
        return drawable;
    }
}
