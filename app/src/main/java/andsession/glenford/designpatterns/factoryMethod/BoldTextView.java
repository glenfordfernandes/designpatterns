package andsession.glenford.designpatterns.factoryMethod;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by glenford on 1/6/16.
 */
public class BoldTextView extends BasicTextView {

    public BoldTextView(Context context) {
        super(context);
    }

    public BoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setStyle() {
        setTypeface(null, Typeface.BOLD);
    }
}
