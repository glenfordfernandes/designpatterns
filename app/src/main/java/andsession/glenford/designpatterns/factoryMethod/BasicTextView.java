package andsession.glenford.designpatterns.factoryMethod;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by glenford on 1/6/16.
 */
abstract public class BasicTextView extends TextView {

    public BasicTextView(Context context) {
        super(context);
        setStyle();
    }

    public BasicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BasicTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    abstract protected void setStyle();

}
