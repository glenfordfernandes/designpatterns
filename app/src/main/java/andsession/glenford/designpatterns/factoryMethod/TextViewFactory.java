package andsession.glenford.designpatterns.factoryMethod;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import andsession.glenford.designpatterns.abstractFactory.AbstractFactory;

/**
 * Created by glenford on 1/6/16.
 */
public class TextViewFactory extends AbstractFactory {

    @Override
    public BasicTextView getStyledTextView(Context context, int style) {

        BasicTextView basicTextView = null;
        switch (style) {
            case Typeface.BOLD:
                basicTextView = new BoldTextView(context);
                break;
            case Typeface.ITALIC:
                basicTextView = new ItalicTextView(context);
                break;
            case Typeface.BOLD_ITALIC:
                basicTextView = new BoltalicTextView(context);
                break;
            default:
                basicTextView = new BasicTextView(context) {
                    @Override
                    protected void setStyle() {
                        setTypeface(null, Typeface.NORMAL);
                    }
                };
                break;
        }
        return basicTextView;
    }

    @Override
    public Drawable getDrawable(Context context, Shape shape) {
        return null;
    }
}
