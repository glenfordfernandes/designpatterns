package andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern;

/**
 * Created by glenford on 6/6/16.
 */
public interface View {
    void setColor();
    void setSize();
}
