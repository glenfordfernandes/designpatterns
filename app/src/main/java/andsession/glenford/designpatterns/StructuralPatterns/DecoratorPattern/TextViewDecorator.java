package andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by glenford on 6/6/16.
 */
public class TextViewDecorator extends TextView implements View {

    protected View mTextView;

    public TextViewDecorator(Context context, View textView) {
        super(context);
        this.mTextView = textView;
    }

    public TextViewDecorator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewDecorator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setColor() {
        mTextView.setColor();
    }

    @Override
    public void setSize() {
        mTextView.setSize();
    }
}
