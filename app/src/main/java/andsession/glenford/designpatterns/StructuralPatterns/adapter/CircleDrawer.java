package andsession.glenford.designpatterns.StructuralPatterns.adapter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by glenford on 20/7/16.
 */
public class CircleDrawer extends View {

    private Paint mPaint;
    private CircleProperty mCircleProperty;

    protected class CircleProperty {

        public int mRadius = 20;
        public int mStartX = 0;
        public int mStartY = 0;

        public CircleProperty(int mRadius, int mStartX, int mStartY) {
            this.mRadius = mRadius;
            this.mStartX = mStartX;
            this.mStartY = mStartY;
        }
    }

    public CircleDrawer(Context context) {
        super(context);
    }

    public CircleDrawer(Context context, CircleProperty circleProperty) {
        super(context);
        init();
        mCircleProperty = circleProperty;
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Paint.Style.FILL);
    }

    public CircleDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(mCircleProperty.mStartX, mCircleProperty.mStartY,
                mCircleProperty.mRadius, mPaint);
    }
}
