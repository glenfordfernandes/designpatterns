package andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

import andsession.glenford.designpatterns.R;

/**
 * Created by glenford on 6/6/16.
 */
public class SimpleTextView extends TextView implements View {

    public SimpleTextView(Context context) {
        super(context);
        setColor();
        setSize();
    }

    public SimpleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SimpleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setColor() {
        setTextColor(Color.BLUE);
    }

    @Override
    public void setSize() {
        setTextSize(getResources().getDimension(R.dimen.size_twelve));
    }
}
