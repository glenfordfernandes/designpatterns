package andsession.glenford.designpatterns.StructuralPatterns.DecoratorPattern;

import android.content.Context;
import android.graphics.Typeface;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by glenford on 6/6/16.
 */
public class DecoratedFontFaceTextView extends TextViewDecorator {

    public DecoratedFontFaceTextView(Context context, View textView) {
        super(context, textView);
        setText(((TextView)textView).getText());
        setColor();
        setFace();
    }

    public DecoratedFontFaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DecoratedFontFaceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setColor() {
        setFace();
        setWebLinkFeature();
        mTextView.setColor();
    }

    @Override
    public void setSize() {
        mTextView.setSize();
    }

    private void setFace() {
        ((TextView)mTextView).setTypeface(null, Typeface.BOLD_ITALIC);
    }

    private void setWebLinkFeature() {
        ((TextView)mTextView).setMovementMethod(new LinkMovementMethod());
    }
}
