package andsession.glenford.designpatterns.StructuralPatterns.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by glenford on 20/7/16.
 */
public class DrawCircleAdapter extends CircleDrawer implements DrawHelper {

    private static final String TAG = DrawCircleAdapter.class.getSimpleName();

    public DrawCircleAdapter(Context context) {
        super(context);
    }

    public DrawCircleAdapter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void draw(View view) {
        CircleProperty circleProperty = new CircleProperty(
                Math.min(view.getHeight() / 2, view.getWidth() / 2),
                        (int) view.getHeight() / 2, (int) view.getWidth() / 2);
        CircleDrawer circleDrawer = new CircleDrawer(view.getContext(), circleProperty);
        if (view instanceof ViewGroup)
            ((ViewGroup) view).addView(circleDrawer);
        else
            Log.d(TAG, "Passed view is not a viewgroup");
    }
}
