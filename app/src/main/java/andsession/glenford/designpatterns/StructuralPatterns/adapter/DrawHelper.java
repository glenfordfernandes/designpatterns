package andsession.glenford.designpatterns.StructuralPatterns.adapter;

import android.view.View;

/**
 * Created by glenford on 20/7/16.
 */
public interface DrawHelper {
    void draw(View view);
}
